# -*- coding: utf-8 -*-

from django.views.generic import TemplateView

# from models import Thread

# TemplateView ti je najjednostavniji za koristit, ali možda će ti trebati i ListView, a 
# to malo prouči u dokumentaciji

class HomeView(TemplateView):
    template_name = "forum/home.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        # threads = Thread.objects.filter().order_by('order')

        # context['threads'] = positions

        return context